﻿/**
 * Copyright (C) 2021  createc-engineering
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __IDACDRIVERSERIAL_H
#define __IDACDRIVERSERIAL_H

//#include <Idac/IdacDriverManager.h> // For SerialHandle
#include <IdacDriverWithThread.h>
#include <QVersionNumber>
#include <QMap>
#include <QMutex>
#include <QTimer>

// These should be used in IdacDriverUsb and derived classes to check USB errors
//#define CHECK_USBRESULT_RET(x) { if ((x) < 0) { logUsbError(__FILE__, __LINE__, x); return; } }
//#define CHECK_USBRESULT_RETVAL(x, ret) { if ((x) < 0) { logUsbError(__FILE__, __LINE__, x); return (ret); } }
//#define CHECK_USBRESULT_NORET(x) { if ((x) < 0) { logUsbError(__FILE__, __LINE__, x); } }


//#define MAX_INTEL_HEX_RECORD_LENGTH 64
//struct INTEL_HEX_RECORD
//{
//	quint32 length;
//	quint32 address;
//	quint32 type;
//	quint8 data[MAX_INTEL_HEX_RECORD_LENGTH];
//};

/* Structure describing firmware info */
typedef struct
{
    QChar           HW_R; // hardware revision character
    QVersionNumber  HW_V; // hardware
    QVersionNumber  BL_V; // bootloader
    QVersionNumber  FW_V; // firmware
} IDAC_SERIAL_FIRMWARE_t;

/*!
 * \brief Enumerator for device commands
 */
typedef enum
{
    IDAC_SERIAL_COMMAND_CMDGOTOSLEEP = 0u,
    IDAC_SERIAL_COMMAND_CMDGOTOBLD,     //1
    IDAC_SERIAL_COMMAND_CMDRSTCPU,      //2
    IDAC_SERIAL_COMMAND_CMDPING,        //3
    IDAC_SERIAL_COMMAND_CMDSMPSTART,    //4
    IDAC_SERIAL_COMMAND_CMDSMPSTOP,     //5
    IDAC_SERIAL_COMMAND_DATVERINFO,     //6
    IDAC_SERIAL_COMMAND_DATBATVOLT,     //7
    IDAC_SERIAL_COMMAND_DATSMP,         //8
    IDAC_SERIAL_COMMAND_SETAINGAIN,     //9
    IDAC_SERIAL_COMMAND_SETAINHPF,      //10
    IDAC_SERIAL_COMMAND_SETAINOFFSET,   //11
    IDAC_SERIAL_COMMAND_SETDOUT,        //12
    IDAC_SERIAL_COMMAND_SETCONFIG,      //13
    IDAC_SERIAL_COMMAND_NR_ENTRIES,
    IDAC_SERIAL_COMMAND_UNDEFINED = -1
} IDAC_SERIAL_COMMAND_Enum;

/*!
 * \brief Enumerator for device response
 */
typedef enum
{
    IDAC_SERIAL_RESPONSE_OK = 0u,
    IDAC_SERIAL_RESPONSE_ERROR,
    IDAC_SERIAL_RESPONSE_NR_ENTRIES,
    IDAC_SERIAL_RESPONSE_UNDEFINED = -1
} IDAC_SERIAL_RESPONSE_Enum;

/*!
 * \brief Stores a KEY VALUE map of the different device command enums.
 */
typedef QMap<QByteArray, IDAC_SERIAL_COMMAND_Enum> IDAC_SERIAL_COMMAND_MAP;

/*!
 * \brief Stores a KEY VALUE map of the device response enums to the read data.
 */
typedef QMap<QByteArray, IDAC_SERIAL_RESPONSE_Enum> IDAC_SERIAL_RESPONSE_MAP;

/*!
 * \brief Enumerator describing communication direction (R/W)
 */
typedef enum
{
    /*!
     * \brief Describes a command type like e.g. DATASMP. This type is received from
     * the device without a specific request. It has a status and a data body.
     */
    IDAC_SERIAL_COM_DIRECTION_RESPONSELESS = 0u,
    /*!
     * \brief Describes a command like e.g. CMDPING, but also setters like
     * e.g. SETDOUT. These types are received after sending an explicit request.
     * This type has a status and a data body.
     */
    IDAC_SERIAL_COM_DIRECTION_COMMAND, // CMD
    /*!
     * \brief Descripes a command like e.g. SETAINGAIN (if used as getter). These types
     * are received after sending an explcicit request. This type has just a status and
     * no data body.
     */
    IDAC_SERIAL_COM_DIRECTION_GET, // GET
    //IDAC_SERIAL_COM_DIRECTION_SET, //SET -- lars 04/12/2021 CMD has same behavior as setter
    IDAC_SERIAL_COM_DIRECTION_NR_ENTRIES,
    IDAC_SERIAL_COM_DIRECTION_UNDEFINED = -1
} IDAC_SERIAL_COM_DIRECTION_Enum;


/*!
 * \brief Structure describing communication handle
 */
typedef struct IDAC_SERIAL_COM_API_t
{
   /*!
    * \brief eCommandID - command ID of the communication handle
    */
    IDAC_SERIAL_COMMAND_Enum eCommandID = IDAC_SERIAL_COMMAND_UNDEFINED;
    /*!
     * \brief eComDirection
     */
    IDAC_SERIAL_COM_DIRECTION_Enum eComDirection = IDAC_SERIAL_COM_DIRECTION_UNDEFINED;
    /*!
     * \brief sendBuffer - buffer for sending data to the device (e.g. for setters)
     */
    QByteArray sendBuffer = nullptr;
} IDAC_SERIAL_COM_API_t;


/*!
 * \brief Structure describing command return structure
 */
typedef struct IDAC_SERIAL_RETURN_t
{
    /*!
     * \brief eSuccess - status of the command (OK or ERROR)
     */
    IDAC_SERIAL_RESPONSE_Enum eSuccess = IDAC_SERIAL_RESPONSE_UNDEFINED;
    /*!
     * \brief abReturnValue - return value of the command
     */
    QByteArray abReturnValue = nullptr;
} IDAC_SERIAL_RETURN_t;


/*!
 * \brief Structure describing the global serial buffer
 */
typedef struct IDAC_SERIAL_BUFFER_t
{
    /*!
     * \brief uiQueueId - unique ID for the serial buffer
     * This ID is used to identify the serial buffer in the serial buffer list.
     * The ID is generated by the serial buffer list.
     * \note The ID is one based, so the first ID is 1.
     */
    uint uiQueueId;
    /*!
     * \brief sRequestCommand
     */
    IDAC_SERIAL_COM_API_t sRequestCommand;
    /*!
     * \brief sReturn
     */
    IDAC_SERIAL_RETURN_t sReturn;
} IDAC_SERIAL_BUFFER_t;



class IdacDriverSerial : public IdacDriverWithThread
{
    Q_OBJECT

public:
    IdacDriverSerial(QObject* parent = NULL);
    ~IdacDriverSerial();

    SerialHandle* serHandle() { return m_serHandle; }

private:
    void vAddSerialBuffer(IDAC_SERIAL_BUFFER_t sSerialBuffer);
    void vRemoveSerialBuffer(uint uiQueueId);
    void vUpdateSerialBuffer(IDAC_SERIAL_COM_API_t* sResponseCommand, IDAC_SERIAL_RETURN_t* sReturn);
//    void vUpdateSerialBuffer(QList<IDAC_SERIAL_COM_API_t>* lsResponseCommand, IDAC_SERIAL_RETURN_t* eStatus);
    QByteArray PrintSerialBuffer();

protected:
    IDAC_SERIAL_RESPONSE_Enum sSendCommand(IDAC_SERIAL_COM_API_t* psCommand);
    QQueue<IDAC_SERIAL_RETURN_t>* m_sampleQueue;
    QMutex* m_sampleQueueMutex = new QMutex(QMutex::Recursive);

protected slots:
    /*!
    * \brief This function receives a sended command. For this the provided command will be send first.
    * After sending the refering command will be expected in serial buffer for defined timeout. If the
    * command is received the function will return IDAC_SERIAL_RESPONSE_OK. If the command is not
    * received the function will return IDAC_SERIAL_RESPONSE_ERROR. The timeout is defined in ms.
    * \param psCommand (default = NULL) if NULL the function will return IDAC_SERIAL_RESPONSE_ERROR
    * \param timeout (default = 1000ms) if timeout is 0 the function will return IDAC_SERIAL_RESPONSE_ERROR
    * \return IDAC_SERIAL_RESPONSE_OK or IDAC_SERIAL_RESPONSE_ERROR
    */
    IDAC_SERIAL_RETURN_t sReceiveCommand(IDAC_SERIAL_COM_API_t* psCommand, uint timeout = 5000);

private slots:
    void vReceiveData();

signals:
    void updateSerialBuffer();

private:
    SerialHandle* m_serHandle;
    QList<IDAC_SERIAL_BUFFER_t>* m_serialBuffer = nullptr;
};

#endif
