/**
 * Copyright (C) 2022 createc-engineering
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "IdacDriverSerial.h"

#include <QtDebug>
#include <QFile>
#include <QTimer>
#include <QEventLoop>
#include <QQueue>
#include <Check.h>
#include <QCoreApplication>

// Debug
#include <QTime>
#include "Sleeper.h"

IDAC_SERIAL_RESPONSE_MAP resultMap;
IDAC_SERIAL_COMMAND_MAP commandMap;

IdacDriverSerial::IdacDriverSerial(QObject* parent)
    : IdacDriverWithThread(parent)
{
    m_serialBuffer = new QList<IDAC_SERIAL_BUFFER_t>;
    m_serHandle = new SerialHandle();
    m_sampleQueue = new QQueue<IDAC_SERIAL_RETURN_t>;

    connect(m_serHandle, SIGNAL(readyRead()), this, SLOT(vReceiveData()));

    resultMap["OK"]             = IDAC_SERIAL_RESPONSE_OK;
    resultMap["ERROR"]          = IDAC_SERIAL_RESPONSE_ERROR;

    commandMap["CMDGOTOSLEEP"]  = IDAC_SERIAL_COMMAND_CMDGOTOSLEEP;
    commandMap["CMDGOTOBLD"]    = IDAC_SERIAL_COMMAND_CMDGOTOBLD;
    commandMap["CMDRSTCPU"]     = IDAC_SERIAL_COMMAND_CMDRSTCPU;
    commandMap["CMDPING"]       = IDAC_SERIAL_COMMAND_CMDPING;
    commandMap["CMDSMPSTART"]   = IDAC_SERIAL_COMMAND_CMDSMPSTART;
    commandMap["CMDSMPSTOP"]    = IDAC_SERIAL_COMMAND_CMDSMPSTOP;
    commandMap["DATVERINFO"]    = IDAC_SERIAL_COMMAND_DATVERINFO;
    commandMap["DATBATVOLT"]    = IDAC_SERIAL_COMMAND_DATBATVOLT;
    commandMap["DATSMP"]        = IDAC_SERIAL_COMMAND_DATSMP;
    commandMap["SETAINGAIN"]    = IDAC_SERIAL_COMMAND_SETAINGAIN;
    commandMap["SETAINHPF"]     = IDAC_SERIAL_COMMAND_SETAINHPF;
    commandMap["SETAINOFFSET"]  = IDAC_SERIAL_COMMAND_SETAINOFFSET;
    commandMap["SETDOUT"]       = IDAC_SERIAL_COMMAND_SETDOUT;
    commandMap["SETCONFIG"]     = IDAC_SERIAL_COMMAND_SETCONFIG;
}

IdacDriverSerial::~IdacDriverSerial()
{
    if (m_serHandle->isOpen()){
        m_serHandle->close();
    }
    if (m_serHandle != NULL){
        delete m_serHandle;
        m_serHandle = NULL;
    }
    delete m_serialBuffer;
    delete m_sampleQueue;

}

/*!
 * \brief This function sends an command to the serial device. Each command creates a new entry within
 * the serial buffer. As a identifiaction the eCommandID will be used.
 * \param psCommand
 * \return
 */
IDAC_SERIAL_RESPONSE_Enum IdacDriverSerial::sSendCommand(IDAC_SERIAL_COM_API_t* psCommand)
{
    IDAC_SERIAL_BUFFER_t sendData;
    // set command id
    sendData.sRequestCommand.eCommandID = psCommand->eCommandID;
    // set command direction
    sendData.sRequestCommand.eComDirection = psCommand->eComDirection;

    qint64 result = m_serHandle->write(psCommand->sendBuffer, qstrlen(psCommand->sendBuffer));

    CHECK_ASSERT_RETVAL(result == psCommand->sendBuffer.size(), IDAC_SERIAL_RESPONSE_ERROR);

    // FIXME: for debug only
    qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
        <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
        << " | psCommand->sendBuffer = "
        << psCommand->sendBuffer;
    // ENDFIX

    // append it to the serial buffer
    vAddSerialBuffer(sendData);

    return IDAC_SERIAL_RESPONSE_OK;
}

/*!
 * \brief This function receives a sended command. For this the provided command will be send first.
 * After sending the refering command will be expected in serial buffer for defined timeout. If the
 * command is received the function will return IDAC_SERIAL_RESPONSE_OK. If the command is not
 * received the function will return IDAC_SERIAL_RESPONSE_ERROR. The timeout is defined in ms.
 * \param psCommand (default = NULL) if NULL the function will return IDAC_SERIAL_RESPONSE_ERROR
 * \param timeout (default = 1000ms) if timeout is 0 the function will return IDAC_SERIAL_RESPONSE_ERROR
 * \return IDAC_SERIAL_RESPONSE_OK or IDAC_SERIAL_RESPONSE_ERROR
 */
IDAC_SERIAL_RETURN_t IdacDriverSerial::sReceiveCommand(IDAC_SERIAL_COM_API_t* psCommand, uint timeout)
{
    QTimer timer;
    timer.setSingleShot(true);
    QEventLoop loop;

    // FIXME: for debug only
    qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
        <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
        << " >> begin receiving for psCommand->eCommandIDt = "
        << psCommand->eCommandID
        << " | timeout = "
        << timeout;
    //ENDFIX

    // the DATSMP is responseless
    IDAC_SERIAL_RETURN_t sResult; // = {IDAC_SERIAL_RESPONSE_ERROR, ""};
    sResult.eSuccess = IDAC_SERIAL_RESPONSE_ERROR;
    sResult.abReturnValue = "";

    sResult.eSuccess = sSendCommand(psCommand);

    //connect the producer
    bool connectUpdate = connect(this, SIGNAL(updateSerialBuffer()), &loop, SLOT(quit()), Qt::DirectConnection);
    CHECK_ASSERT_NORET(connectUpdate);

    //connect the timer that will throw the timeout
    bool connectTimeout = connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()), Qt::DirectConnection);
    CHECK_ASSERT_NORET(connectTimeout);

    timer.start(timeout);
    loop.exec();

    if (!timer.isActive()){
        // TODO: set error message - communication timeout
        sResult.eSuccess = IDAC_SERIAL_RESPONSE_ERROR;
        sResult.abReturnValue = "";

        // FIXME: for debug only
        qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
            <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
            << " << escape receiving for psCommand->eCommandIDt = "
            << psCommand->eCommandID
            << " | !timer.isActive() ";
        //ENDFIX

        return sResult; // {IDAC_SERIAL_RESPONSE_ERROR, ""};
    }

    // if serial buffer was updated

    // FIXME: for debug only
    qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
             <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
             << " | m_serialBuffer->size() = "
             << m_serialBuffer->size();
    //ENDFIX

    for (uint i = m_serialBuffer->size(); i > 0; i--){
        // check serial buffer for refering command
        if (m_serialBuffer->at(i-1).sRequestCommand.eCommandID == psCommand->eCommandID){

            /* If we have a command type that returns data, we return the data */
            if (psCommand->eComDirection > IDAC_SERIAL_COM_DIRECTION_COMMAND){
                sResult.abReturnValue = m_serialBuffer->at(i-1).sReturn.abReturnValue;
            }

            sResult.eSuccess = IDAC_SERIAL_RESPONSE_OK;
            // remove from buffer

            // FIXME: for debug only
            qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
                <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
                << " | m_serialBuffer->at(i-1).sRequestCommand.eCommandID = "
                << m_serialBuffer->at(i-1).sRequestCommand.eCommandID
                << " | i-1 = "
                << i-1;
            //ENDFIX

            //m_serialBuffer->removeAt(i-1);
            vRemoveSerialBuffer(i);

            // the command was consumed
            goto exit; // Prevent doubling of the return path
        } else {

            // FIXME: for debug only
            qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
                <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
                << " | psCommand->eCommandID = "
                << psCommand->eCommandID
                << " | ELSE CASE!";
            // ENDFIX
        }
    }
    exit:

    // FIXME: for debug only
    qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
        <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
        << " << end receiving | PrintSerialBuffer = "
        << PrintSerialBuffer();
    // ENDFIX

    return sResult;
}

/*!
 * \brief This function is connected to the readyRead signal of the opened serial port.
 * A complete received command is always done with an ending OK\r or ERROR\r.
 */
void IdacDriverSerial::vReceiveData()
{
    // FIXME: for debug only
    qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
             <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
             << " | >>> begin vReceiveData()";
    // ENDFIX

    static QByteArray abBuffer = nullptr;
    // 1. fill the static buffer with received data
    abBuffer.append(m_serHandle->readAll());

    // FIXME: for debug only
    qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
        <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
        << " | abBuffer = "
        << abBuffer;
    // ENDFIX

    /*!
     * \brief labEntrys
     */
    QList<QByteArray> labEntrys = abBuffer.split('\r');

    /*  Because of the division of the buffer into a list, we must have a size of 2 to move forward.
    The split method always appends at least one empty entry if the token was found.
    This means that there is no complete response within the buffer. */
    if (1 >= labEntrys.size()){
        // continue receiving
        // FIXME: for debug only
        qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
                 <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
                 << " << (1 >= labEntrys.size())";
        // ENDFIX

        return;
    }
    /* To prevent multiple use of data that has already been evaluated, it must be deleted from the static buffer. */
    else {
        int iCounter = 0;
        for (QByteArray &e : labEntrys){
            iCounter++;
            /* Remove each complete entry from the binning, if the received data is without overhang.
             * The last entry of the split() method will be empty and can be removed. */
            if (iCounter != labEntrys.size() && !e.isEmpty()){
                abBuffer.remove(0, e.size()+1);
            }
        }
    }

    if (labEntrys.last() == ""){
        labEntrys.removeLast();
    }

    // stores the data if available
    bool bCommandComplete = false;
    QByteArray abData = nullptr;
    // the command description
    IDAC_SERIAL_COM_API_t sResponseCommand;
    // the result description
    IDAC_SERIAL_RETURN_t sReturn;

    // CHECK
    /* The device has timed out and is restarted with its device identifier started to transmit again. */

    // 2. check if a complete command are in the buffer
    for (int i = 0; i < labEntrys.size(); i++){
        bool bUpdateBuffer = true;

        /* Perform some resets because a complete command was consumed in the previous iteration */
        if (bCommandComplete){
            abData = nullptr;
            sResponseCommand = {};
            sReturn = {};
            bCommandComplete = false;
        }

        // Fullfilled command response
        if (resultMap.contains(labEntrys.at(i))){
            // Type of commands that return only a status, will be overwritten if data available
            if (IDAC_SERIAL_COM_DIRECTION_UNDEFINED == sResponseCommand.eComDirection){
                sResponseCommand.eComDirection = IDAC_SERIAL_COM_DIRECTION_COMMAND;
            }
            /* Since data is present, it is now a different command type. The data is now
             * appended and the requested command is identified by extraction from the data.*/
            if (nullptr != abData){
                sReturn.abReturnValue = abData;
            }
            sReturn.eSuccess = resultMap[labEntrys.at(i)];

            // Sample data wont be add to the serial buffer
            if (sResponseCommand.eCommandID == IDAC_SERIAL_COMMAND_DATSMP){
                // There is a final precondition in case sampling has stopped but overhanging data arrives.
                // In this case, updating the buffer is not desired.
                if (m_bSampling == true){

                    m_sampleQueueMutex->lock();

                    int iSize = m_sampleQueue->size();

                    m_sampleQueue->enqueue(sReturn);

                    CHECK_ASSERT_NORET(iSize == ( m_sampleQueue->size() - 1 ) );

                    m_sampleQueueMutex->unlock();

                }
                bUpdateBuffer = false;
                bCommandComplete = true;
            }

            if (bUpdateBuffer){

                // FIXME: for debug only
                qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
                    <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
                    << " | sResponseCommand.eCommandID = "
                    << sResponseCommand.eCommandID
                    << " | sReturn.eSuccess = "
                    << sReturn.eSuccess
                    << " | sReturn.abReturnValue = "
                    << sReturn.abReturnValue;
                // ENDFIX

                // Send the command to the serial buffer
                vUpdateSerialBuffer(&sResponseCommand, &sReturn);
                bCommandComplete = true;
            }
        } else {
            // collect the data that will be send if status were collected
            QList<QByteArray> abHelper = labEntrys.at(i).split(':');

            CHECK_ASSERT_RET(1 < abHelper.size() )

            abData = abHelper[1];
            QList<QByteArray> commandMapKeys = commandMap.keys();
            // to identify the type of command received, the entire command is extracted, concatenated by a colon
            for (QByteArray &e : commandMapKeys){
                // a lookup table is used to match the extracted identifier
                if (abHelper[0].contains(e)){
                    sResponseCommand.eCommandID = commandMap.value(e);
                    // since we are collecting data, a GET command was received
                    sResponseCommand.eComDirection = IDAC_SERIAL_COM_DIRECTION_GET;
                }
            }
        }
    }

    // FIXME: for debug only
    qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
             <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
             << " | <<< end vReceiveData()";
    // ENDFIX

    CHECK_ASSERT_NORET(m_serHandle->readAll().isEmpty());
}

/*!
 * \brief This function is used to add a command to the serial buffer.
 * \param sSerialBuffer The command to add.
 */
void IdacDriverSerial::vAddSerialBuffer(IDAC_SERIAL_BUFFER_t sSerialBuffer)
{
    int preSize = m_serialBuffer->count();

    // FIXME: for debug only
    qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
        <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
        << " | sSerialBuffer.sRequestCommand.eCommandID = "
        << sSerialBuffer.sRequestCommand.eCommandID
        << " | sSerialBuffer.sRequestCommand.eComDirection = "
        << sSerialBuffer.sRequestCommand.eComDirection
        << " | sSerialBuffer.sReturn.abReturnValue = "
        << sSerialBuffer.sReturn.abReturnValue
        << " | m_serialBuffer->count() = "
        << m_serialBuffer->count();
    // ENDFIX

    // set next queue id
    sSerialBuffer.uiQueueId = m_serialBuffer->count()+1;
    m_serialBuffer->append(sSerialBuffer);

    CHECK_ASSERT_RET(m_serialBuffer->count() == ++preSize)

}

/*!
 * \brief This function is used to remove a command from the serial buffer.
 * \note The ID is one based, so the first ID is 1.
 * \param uiQueueId The queue ID of the command to remove.
 */
void IdacDriverSerial::vRemoveSerialBuffer(uint uiQueueId){

    CHECK_PRECOND_RET(1 <= uiQueueId)

    int preSize = m_serialBuffer->count();

    CHECK_PRECOND_RET(0 < preSize)

    for (int i = 0; i < preSize; i++){
        if (m_serialBuffer->at(i).uiQueueId == uiQueueId ){
            m_serialBuffer->removeAt(i);
        }
    }

    CHECK_ASSERT_RET(m_serialBuffer->count() == --preSize)

}

/*!
 * \brief This function is be used to update the global serial buffer with the response value.
 * This is done by searching the buffer for the matching command ID. If there is no match,
 * a second attempt is made for the command direction.
 * \param sResponseCommand Structure of command taken as reference to identicate the command to update.
 * \param sReturn The value that will be added to the buffer.
 */
void IdacDriverSerial::vUpdateSerialBuffer(IDAC_SERIAL_COM_API_t* sResponseCommand, IDAC_SERIAL_RETURN_t* sReturn)
{
    uint size = m_serialBuffer->size();

    // FIXME: for debug only
    qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
        <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
        << " | PrintSerialBuffer = "
        << PrintSerialBuffer();
    // ENDFIX

    CHECK_PRECOND_RET(0 < size);

    for (uint i = 0; i < size; i++){
        if (m_serialBuffer->at(i).sRequestCommand.eCommandID == sResponseCommand->eCommandID){
            m_serialBuffer->operator[](i).sReturn.eSuccess = sReturn->eSuccess;
            m_serialBuffer->operator[](i).sReturn.abReturnValue = sReturn->abReturnValue;

            // FIXME: for debug only
            qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
                <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
                << " | m_serialBuffer->operator[](i).sRequestCommand.eCommandID = "
                << m_serialBuffer->operator[](i).sRequestCommand.eCommandID
                << " | sResponseCommand->eCommandID = "
                << sResponseCommand->eCommandID;
            // ENDFIX

            emit updateSerialBuffer();
            return;
        }
    }

    for (uint i = 0; i < size; i++){
        if (m_serialBuffer->at(i).sRequestCommand.eComDirection == sResponseCommand->eComDirection){
            m_serialBuffer->operator[](i).sReturn.eSuccess = sReturn->eSuccess;
            m_serialBuffer->operator[](i).sReturn.abReturnValue = sReturn->abReturnValue;

            // FIXME: for debug only
            qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
                <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
                << " | m_serialBuffer->operator[](i).sRequestCommand.eComDirection = "
                << m_serialBuffer->operator[](i).sRequestCommand.eComDirection
                << " | sResponseCommand->eCommandID = "
                << sResponseCommand->eCommandID;
            // ENDFIX

            emit updateSerialBuffer();
            return;
        }
    }

    QString s = QString("method: %0\n"
                        "buffer size: %1\n"
                        "sResponseCommand->eComDirection: %2\n"
                        "sResponseCommand->eCommandID: %3\n"
                        "sReturn->abReturnValue: %4\n"
                        "sReturn->eSuccess: %5\n")
                    .arg(__func__)
                    .arg(size)
                    .arg(sResponseCommand->eComDirection)
                    .arg(sResponseCommand->eCommandID)
                    .arg(sReturn->abReturnValue.toInt())
                    .arg(sReturn->eSuccess);
    checkLog(__FILE__, __LINE__, "SERIAL ERROR", s);
    CHECK_ASSERT_RET(false);
}

/*!
 * \brief This function is be used to update the global serial buffer with the response value.
 * \param List of types of commands taken as reference to identicate the command to update.
 * \param The value that will be added to the buffer.
 */
// void IdacDriverSerial::vUpdateSerialBuffer(QList<IDAC_SERIAL_COM_API_t>* lsResponseCommand, IDAC_SERIAL_RETURN_t* eStatus){
//     uint size = m_serialBuffer->size();
//     CHECK_PRECOND_RET(0 < size);

//     for (IDAC_SERIAL_COM_API_t sResponseCommand : qAsConst(*lsResponseCommand)){
//         vUpdateSerialBuffer(&sResponseCommand, eStatus);
//     }

// }

QByteArray IdacDriverSerial::PrintSerialBuffer(){
    QByteArray r;
    r.append("[ ");
    int size = m_serialBuffer->count();

    for (int i = 0; i < size; i++){
        r.append(m_serialBuffer->at(i).sRequestCommand.eCommandID);
        r.append(' ');
        r.append(m_serialBuffer->at(i).sRequestCommand.eComDirection);
        r.append(" | ");
    }
    r.append(" ]");
    return r;
}
