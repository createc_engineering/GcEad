/**
 * Copyright (C) 2022  createc-engineering
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "IdacDriverBT2.h"
#include <Idac/IdacDriverManager.h>
#include <Idac/IdacFactory.h>

#include <QtDebug>
#include <QDir>
#include <Check.h>

/* New includes */
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QRegExp>
#include <QQueue>

/* Debug */
#include <QTime>
#include <QThread>

#define IDACSTART_NOIDACBOARD	0
#define IDACSTART_NOERR			1
#define IDACSTART_TIMEOUT		2
#define IDACSTART_NOBINFILE		3
#define MAXSIGNALS_V2_V3        4

int dwRangesList[IDAC_SCALERANGECOUNT+1] = {
    MAX_INPUT_VOLTAGE_ADC / 1,
    MAX_INPUT_VOLTAGE_ADC / 4,
    MAX_INPUT_VOLTAGE_ADC / 16,
    MAX_INPUT_VOLTAGE_ADC / 64,
    MAX_INPUT_VOLTAGE_ADC / 256,
    MAX_INPUT_VOLTAGE_ADC / 1024,
    -1
};

IDAC_SERIAL_FIRMWARE_t minFW{
    'A',
    QVersionNumber(0 , 0),
    QVersionNumber(2 , 0),
    QVersionNumber(1, 0)
};

IDAC_SERIAL_FIRMWARE_t maxFW{
    'A',
    QVersionNumber(0 , 0),
    QVersionNumber(2 , 0),
    QVersionNumber(1, 0)
};

IDAC_SERIAL_COM_API_t icPing;
IDAC_SERIAL_COM_API_t icSampleStart;
IDAC_SERIAL_COM_API_t icSampleStop;
IDAC_SERIAL_COM_API_t icDateiVersion;
IDAC_SERIAL_COM_API_t icDatSmp;
IDAC_SERIAL_COM_API_t icSetGain;
IDAC_SERIAL_COM_API_t icSetHpf;
IDAC_SERIAL_COM_API_t icSetOffset;
IDAC_SERIAL_COM_API_t icBatteryVoltage;

IdacDriverBT2::IdacDriverBT2(QObject* parent)
    : IdacDriverSerial(parent),
    m_defaultChannelSettings(3)
{
    icPing.eCommandID = IDAC_SERIAL_COMMAND_CMDPING;
    icPing.eComDirection = IDAC_SERIAL_COM_DIRECTION_COMMAND;
    icPing.sendBuffer = "CMDPING\r";

    icSampleStart.eCommandID = IDAC_SERIAL_COMMAND_CMDSMPSTART;
    icSampleStart.eComDirection = IDAC_SERIAL_COM_DIRECTION_COMMAND;
    icSampleStart.sendBuffer = "CMDSMPSTART\r";

    icSampleStop.eCommandID = IDAC_SERIAL_COMMAND_CMDSMPSTOP;
    icSampleStop.eComDirection = IDAC_SERIAL_COM_DIRECTION_COMMAND;
    icSampleStop.sendBuffer = "CMDSMPSTOP\r";

    icDateiVersion.eCommandID = IDAC_SERIAL_COMMAND_DATVERINFO;
    icDateiVersion.eComDirection = IDAC_SERIAL_COM_DIRECTION_GET;
    icDateiVersion.sendBuffer = "DATVERINFO?\r";

    icDatSmp.eCommandID = IDAC_SERIAL_COMMAND_DATSMP;
    icDatSmp.eComDirection = IDAC_SERIAL_COM_DIRECTION_GET;
    icDatSmp.sendBuffer = "";

    icSetGain.eCommandID = IDAC_SERIAL_COMMAND_SETAINGAIN;
    icSetGain.eComDirection = IDAC_SERIAL_COM_DIRECTION_COMMAND;
    icSetGain.sendBuffer = "SETAINGAIN";

    icSetHpf.eCommandID = IDAC_SERIAL_COMMAND_SETAINHPF;
    icSetHpf.eComDirection = IDAC_SERIAL_COM_DIRECTION_COMMAND;
    icSetHpf.sendBuffer = "SETAINHPF";

    icSetOffset.eCommandID = IDAC_SERIAL_COMMAND_SETAINOFFSET;
    icSetOffset.eComDirection =  IDAC_SERIAL_COM_DIRECTION_COMMAND;
    icSetOffset.sendBuffer = "SETAINOFFSET";

    icBatteryVoltage.eCommandID = IDAC_SERIAL_COMMAND_DATBATVOLT;
    icBatteryVoltage.eComDirection = IDAC_SERIAL_COM_DIRECTION_GET;
    icBatteryVoltage.sendBuffer = "DATBATVOLT?\r";

    m_manager = IdacFactory::getDriverManager(true);
    m_proxy = IdacFactory::getProxy();

    m_tHeartbeatTimer = new QTimer;
    m_tQueueTimer = new QTimer;
    m_commandQueue = new QQueue<IDAC_SERIAL_COM_API_t>;

    //m_serPort = serPort;
    m_bFirmwareReady = false;
    m_bIdac2 = false;
    m_bIdac4 = false;

    setHardwareName("IDAC2_BT");

    initDefaultChannelSettings();

    /* Prepare heartbeat interrupt */
    connect(m_tHeartbeatTimer, SIGNAL(timeout()), this, SLOT(eOnHeartbeatTimeout()));
    m_tHeartbeatTimer->start(5000);

    /* Prepare command cueue */
    connect(m_tQueueTimer, &QTimer::timeout, this, &IdacDriverBT2::vCommandDispatcher);
    m_tQueueTimer->start(5);
}

IdacDriverBT2::~IdacDriverBT2()
{
//	if (m_bFirmwareSent) {
//		IdacPowerDown();
//		IdacUnlock();
//	}
    delete m_tHeartbeatTimer;
    delete m_tQueueTimer;
    delete m_commandQueue;
}


void IdacDriverBT2::vCommandDispatcher(){

    if (!m_commandQueue->isEmpty()){

        IDAC_SERIAL_COM_API_t queuedCommand = m_commandQueue->dequeue();

        // FIXME: for debug only
        qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
            <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
            << " | queuedCommand.eCommandID : "
            << queuedCommand.eCommandID;
        //ENDFIX

        IDAC_SERIAL_RETURN_t result = sReceiveCommand(&queuedCommand);

        // TODO: add exception handling

        switch (queuedCommand.eCommandID) {
            case IDAC_SERIAL_COMMAND_DATBATVOLT: {
                m_vbat = QString(result.abReturnValue).toFloat();
                break;
            }
            default : { }
        }
    }

}

void IdacDriverBT2::loadCaps(IdacCaps* caps)
{
    CHECK_PARAM_RET(caps != NULL);

    caps->bHighcut = false;
    // 5.6.3 SETAINOFFSET?
    caps->bRangePerChannel = true;

}

void IdacDriverBT2::initDefaultChannelSettings()
{
    QVector<IdacChannelSettings>& channels = m_defaultChannelSettings;

    channels[0].mEnabled = 0x03;
    channels[0].mInvert = 0x00;
    channels[0].nDecimation = -1;

    channels[1].mEnabled = 1;
    channels[1].mInvert = 0;
    channels[1].nDecimation = -1;
    channels[1].iRange = 1;
    channels[1].iHighcut = -1;
    channels[1].iLowcut = 1; // 0.02 Hz on IDAC2_BT
    channels[1].nExternalAmplification = 1;

    channels[2].mEnabled = 1;
    channels[2].mInvert = 0;
    channels[2].nDecimation = -1;
    channels[2].iRange = 1;
    channels[2].iHighcut = -1;
    channels[2].iLowcut = 1; // 0.02 Hz on IDAC2_BT
    channels[2].nExternalAmplification = 1;

}

bool IdacDriverBT2::checkUsbFirmwareReady()
{
    // We return a true even though we don't have a USD driver so that the manager does not initialize a different USB connection
    return true;
}

bool IdacDriverBT2::checkDataFirmwareReady()
{
    return m_bFirmwareReady;
}

void IdacDriverBT2::initUsbFirmware()
{
}

void IdacDriverBT2::initStringsAndRanges()
{
    setLowcutStrings(QStringList() <<
        "   DC" <<
        "0.1Hz" <<
        "0.2Hz" <<
        "0.3Hz" <<
        "0.5Hz" <<
        "  1Hz" <<
        "  2Hz" <<
        "  3Hz" <<
        "  5Hz" <<
        " 16Hz"
    );

    QList<int> list;
    int* ranges = dwRangesList;
    while (*ranges != -1)
        list << *ranges++;
    setRanges(list);
}

void IdacDriverBT2::initDataFirmware()
{
    /* Check if the version ID is OK */
    IDAC_SERIAL_RETURN_t sResult = sReceiveCommand(&icDateiVersion);
    // TODO: finish QVersionNumber implementation
//    QList<QByteArray> fwBuffer = sResult.abReturnValue.split(',');
//    IDAC_SERIAL_FIRMWARE_t fw = {
//        fwBuffer.at(0).toInt(),
//        QVersionNumber(fwBuffer.at(1).toInt(), fwBuffer.at(2).toInt()),
//        QVersionNumber(fwBuffer.at(3).toInt(), fwBuffer.at(4).toInt()),
//        QVersionNumber(fwBuffer.at(5).toInt(), fwBuffer.at(6).toInt()),
//    };

    // Format of the IDAC version information
    QRegExp rx("[A-Z],\\d,\\d,\\d,\\d,\\d,\\d");
    /* Get the IDAC version information */
    if (rx.indexIn(sResult.abReturnValue) != -1){

        /* Initialization checks were ok, so the firmware is ready */
        m_bFirmwareReady = true;

        /* Put the event of connection with the recorded fw version of the device to the log */
        LOG(1, QString("IDAC-BT2 with firmware version: %1 connected").arg(QString(sResult.abReturnValue)));
    }

    initStringsAndRanges();

    /* Get device battery voltage */
    m_commandQueue->enqueue(icBatteryVoltage);

//	m_bFirmwareSent = boot();
//	if (!m_bFirmwareSent) {
//		IdacUnlock();
//		return;
//	}

//	// Get range information from the hardware
//	LPDWORD pRanges = IdacGetRanges();
//	QList<int> ranges;
//	for (LPDWORD pnRange = pRanges; int(*pnRange) != -1; pnRange++)
//	{
//		ranges << *pnRange;
//	}
//	setRanges(ranges);
//	qDebug() << ranges;

//	// Get low-pass filter information from the hardware
//	int nStrings;
//	const char** asStrings;
//	IdacLowPassStrings(&nStrings, &asStrings);
//	QStringList list;
//	for (int i = 0; i < nStrings; i++)
//		list << asStrings[i];
//	setHighcutStrings(list);

//	// Get high-pass filter information from the hardware
//	IdacHighPassStrings(&nStrings, &asStrings);
//	list.clear();
//	for (int i = 0; i < nStrings; i++)
//		list << asStrings[i];
//	setLowcutStrings(list);
}

IDAC_SERIAL_RESPONSE_Enum IdacDriverBT2::eOnHeartbeatTimeout()
{
    IDAC_SERIAL_RESPONSE_Enum result = IDAC_SERIAL_RESPONSE_ERROR;

    static int iHeartbeatCounter = 1;

    if (m_bFirmwareReady){
        // The function performs the ping, but also sends the battery voltage every 10th ping
        if (iHeartbeatCounter++ % 10 == 0){
            m_commandQueue->enqueue(icBatteryVoltage);
            // Reset the counter
            iHeartbeatCounter = 1;
        }
        else {
            m_commandQueue->enqueue(icPing);
        }

        // FIXME: for debug only
        qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
            <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
            /*<< " | result = "
            << result*/;
        // ENDFIX
    }

    return result;
}

bool IdacDriverBT2::boot()
{
    return false;
}

bool IdacDriverBT2::boot_2000_4(const QString& sType)
{
    return false;
}

bool IdacDriverBT2::boot_2_USB()
{
    return true;
}

void IdacDriverBT2::configureChannel(int iChan)
{
    const IdacChannelSettings* chan = desiredChannelSettings(iChan);
    CHECK_ASSERT_RET(chan != NULL);

    if (iChan > 0) {
        // iChan [1] --> IDAC channel 0
        // iChan [2] --> IDAC channel 1
        setChannelRange(iChan - 1, chan->iRange);
        setChannelHighPass(iChan - 1, chan->iLowcut);
        setChannelOffset(iChan - 1, chan->nOffset);
    }
//	IdacLowPass(iChan, chan->iHighcut);
//	IdacHighPass(iChan, chan->iLowcut);
//	IdacSetOffsetAnalogIn(iChan, chan->nOffset);
}

// This function will set the range for the given channel
void IdacDriverBT2::setChannelRange(int iChan, int index)
{
    // FIXME: for debug only
    qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
        <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
        << "Set channel: " << iChan << ", " << index;

    CHECK_PARAM_RET(iChan >= 0 || iChan < IDAC_CHANNELCOUNT);
    CHECK_PARAM_RET(index >= 0 || iChan < IDAC_SCALERANGECOUNT);

    IDAC_SERIAL_COM_API_t setChannelGainCmd = icSetGain;
    setChannelGainCmd.sendBuffer.append(QString("%1=%2\r").arg(iChan).arg(index));

    m_commandQueue->enqueue(setChannelGainCmd);
}

// Set the cutoff frequency on the high pass filter for a given channel
void IdacDriverBT2::setChannelHighPass(int iChan, int index) {

    // FIXME: for debug only
    qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
        <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
        << " | Set channel HPF: " << iChan << ", " << index;
    // ENDFIX

    CHECK_PARAM_RET(iChan >= 0 || iChan < IDAC_CHANNELCOUNT);
    CHECK_PARAM_RET(index >= 0 || index < IDAC_HIGHPASSCOUNT);
    IDAC_SERIAL_COM_API_t setChannelHpfCmd = icSetHpf;
    setChannelHpfCmd.sendBuffer.append(QString("%1=%2\r").arg(iChan).arg(index));

    m_commandQueue->enqueue(setChannelHpfCmd);
}

// Set the offset of a given channel
void IdacDriverBT2::setChannelOffset(int iChan, quint32 Offset) {

    // FIXME: for debug only
    qDebug() << "[" << QTime::currentTime().toString("hh:mm:ss,zzz") << "]"
        <<__FILENAME__<<":"<<__LINE__<<"::"<<__func__<<"["<<QThread::currentThreadId()<<"]"
        << " | Set channel Offset: " << iChan << ", " << Offset;
    // ENDFIX

    CHECK_PARAM_RET(iChan >= 0 || iChan < IDAC_CHANNELCOUNT);
    IDAC_SERIAL_COM_API_t setChannelOffsetCmd = icSetOffset;
    setChannelOffsetCmd.sendBuffer.append(QString("%1=%2\r").arg(iChan).arg(Offset, 0, 16));

    m_commandQueue->enqueue(setChannelOffsetCmd);
}

bool IdacDriverBT2::startSampling()
{
    for (int iChan = 0; iChan < 3; iChan++)
    {
        const IdacChannelSettings* chan = desiredChannelSettings(iChan);
        CHECK_ASSERT_RETVAL(chan != NULL, false);

//		IdacEnableChannel(iChan, chan->mEnabled);
//		IdacSetDecimation(iChan, chan->nDecimation);
        if (iChan != 0) configureChannel(iChan);
    }

    startSamplingThread();
    //startSamplingThread(serHandle());
    return true;
}

/* This function will be run in the sample thread */
void IdacDriverBT2::sampleLoop() {
    m_commandQueue->enqueue(icSampleStart);
    //emit idacReceiveCommand(&icSampleStart);
    //sReceiveCommand(&icSampleStart);
    while (m_bSampling) {
        grabDataFromDll();
        msleep(45);
    }
    m_sampleQueue->clear();
    m_commandQueue->enqueue(icSampleStop);
    //emit idacReceiveCommand(&icSampleStop);
    //sReceiveCommand(&icSampleStop);
}

void IdacDriverBT2::grabDataFromDll()
{
    IDAC_SERIAL_RETURN_t sSample;
    if (!m_sampleQueue->isEmpty()){

        m_sampleQueueMutex->lock();

        int iSize = m_sampleQueue->size();

        sSample = m_sampleQueue->dequeue();

        CHECK_ASSERT_NORET(iSize == ( m_sampleQueue->size() + 1) );

        m_sampleQueueMutex->unlock();

    } else {
        return;
    }
    // Check if a valid DATSMP were received
    //IDAC_SERIAL_RETURN_t sSample = emit idacReceiveCommand(&icDatSmp);
    //IDAC_SERIAL_RETURN_t sSample = sReceiveCommand(&icDatSmp);

//    CHECK_PRECOND_RET(sSample.eSuccess != IDAC_SERIAL_RESPONSE_ERROR &&
//                      sSample.eSuccess != IDAC_SERIAL_RESPONSE_UNDEFINED &&
//                      sSample.abReturnValue != "");

    static int iSeqNum = 0;
    bool bOflow = false;
    short analog1 = 0;
    short analog2 = 0;
    short digital = 0;

    QList<QByteArray> lbaData = sSample.abReturnValue.split(',');
    iSeqNum = lbaData[0].toInt();
    bOflow = lbaData[1].toInt();

    /* An array of 10 samples sets are send at once. At a sample rate of 200Hz,
     * this is a period of 50ms.
     *
     * Sample:
     * <seqnum>, <oflow>,
     * <AIN0>, <AIN1>, <DINTRIG>, <DINSTIM> // 1
     * .
     * .
     * .
     * <AIN0>, <AIN1>, <DINTRIG>, <DINSTIM> // 10
     */
    for(int i=2; i <= 41; i+=4){
        bool valid;
        analog1 = lbaData[i].toUInt(&valid,16); // <AIN0>
        analog2 = lbaData[i+1].toUInt(&valid,16); // <AIN1>
        digital |= lbaData[i+2].toShort(); // <DINTRIG>
        digital |= lbaData[i+3].toShort() << 1; // <DINSTIM>

        addSample(digital, analog1, analog2);
    }
}
