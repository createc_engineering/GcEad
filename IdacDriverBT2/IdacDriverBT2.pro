QT -= gui
QT += serialport
QT += core

CONFIG += warn_on \
    staticlib \
    create_prl \
    debug_and_release
TEMPLATE = lib
INCLUDEPATH += . \
    .. \
    ../Core \
    ../IdacDriver

#CONFIG(debug, debug|release):DESTDIR = ../debug
#else:DESTDIR = ../release

HEADERS += \
    IdacDriverBT2Defines.h \
    IdacDriverBT2.h

SOURCES +=  \
    IdacDriverBT2.cpp
