/**
 * Copyright (C) 2021  Createc-Engineering
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __IdacDriverBT2_H
#define __IdacDriverBT2_H

#include <QtGlobal> // for quint8 and related types
#include <QTimer>
#include <IdacDriver/IdacDriverSerial.h>
#include <Idac/IdacDriverManager.h>
#include <Idac/IdacProxy.h>

#define IDAC_CHANNELCOUNT       2
#define IDAC_HIGHPASSCOUNT      9
#define IDAC_SCALERANGECOUNT    6
#define MAX_INPUT_VOLTAGE_ADC   10000000		// full scale is 10V   ( 20Vpp)


#define FULL_BATTERY_VOLTAGE    4.2  // volt
#define EMPTY_BATTERY_VOLTAGE   3.0  // volt

class IdacDriverBT2 : public IdacDriverSerial
{
	Q_OBJECT

public:
    IdacDriverBT2(QObject* parent = NULL);
	~IdacDriverBT2();

// IdacDriver overrides
public:
	/// Load up the capabilities of the current driver
	virtual void loadCaps(IdacCaps* caps);
	/// Load up default channel settings for the current driver
	const QVector<IdacChannelSettings>& defaultChannelSettings() const { return m_defaultChannelSettings; }

	virtual bool checkUsbFirmwareReady();
	virtual bool checkDataFirmwareReady();

	virtual void initUsbFirmware();
	virtual void initDataFirmware();
	virtual void initStringsAndRanges();

	virtual bool startSampling();
	virtual void configureChannel(int iChan);

    // IdacDriverWithThread overrides
    void setHeartbeatTimer(QTimer *newHeartbeatTimer);

    QTimer *tHeartbeatTimer() const;

    QQueue<IDAC_SERIAL_COM_API_t> *commandCueue() const;

    void setChannelRange(int iChan, int index);
    void setChannelHighPass(int iChan, int index);
    void setChannelOffset(int iChan, quint32 Offset);

protected:
	virtual void sampleLoop();
    QQueue<IDAC_SERIAL_RETURN_t> *sampleQueue() const;

private:
	void initDefaultChannelSettings();
	bool boot();
	bool boot_2000_4(const QString& sType);
	bool boot_2_USB();
	//bool boot_ISA_USB();
	//int bootAtAddress(int addr, int nChannels);
	//QString getBinFileName(int nSignals);
	void grabDataFromDll();

private slots:
    IDAC_SERIAL_RESPONSE_Enum eOnHeartbeatTimeout();
    void vCommandDispatcher();

signals:
    IDAC_SERIAL_RETURN_t idacReceiveCommand(IDAC_SERIAL_COM_API_t* psCommand, uint timeout = 1000);

private:
	QVector<IdacChannelSettings> m_defaultChannelSettings;
    bool m_bFirmwareReady;
	bool m_bIdac2;
	bool m_bIdac4;
    QTimer *m_tHeartbeatTimer = nullptr;
    QTimer *m_tQueueTimer = nullptr;
    QQueue<IDAC_SERIAL_COM_API_t>* m_commandQueue = nullptr;
    // see IdacDriverUsbEs.h
    QPointer<IdacDriverManager> m_manager;
    QPointer<IdacProxy> m_proxy;

};

#endif
