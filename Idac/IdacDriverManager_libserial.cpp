/**
 * Copyright (C) 2022  createc-engineering
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "IdacDriverManager.h"

//#if defined(Q_OS_UNIX)

#include <QtDebug>

#include <Check.h>

#include <IdacDriver/IdacDriver.h>
#include <IdacDriver/Sleeper.h>
#include <IdacDriverBT2/IdacDriverBT2.h>
#include <QSerialPort>
#include <QSerialPortInfo>

const QString hwIdentifier= "IDAC2_BT";


//QByteArray m_readData;
//QTextStream m_standardOutput;
//QTimer m_timer;

//void handleReadyRead();
//void handleTimeout();
//void handleError(QSerialPort::SerialPortError serialPortError);


void IdacDriverManager::initLibSerial()
{
    //libusb_init();
    /* do nothing */
#ifndef QT_NO_DEBUG
    //libusb_set_debug(NULL, 3);
#endif
}

void IdacDriverManager::exitLibSerial()
{
    /* do nothing */

    if (m_driver != NULL)
    {
        delete m_driver;
        m_driver = NULL;
    }

//    if (m_serialPort != NULL)
//    {
//        m_serialPort->close();;
//        m_serialPort = NULL;
//    }
}

void IdacDriverManager::createLibSerialDriver()
{
    IdacDriverBT2* serial_driver = new IdacDriverBT2();
    SerialHandle* serialPort = serial_driver->serHandle();
    QTextStream standardOutput(stdout);

    QByteArray readData;
    QByteArray writeData("OK\r");

    QList<QSerialPortInfo> serialPortNames = QSerialPortInfo::availablePorts();
    const int serialPortBaudRate = QSerialPort::Baud115200;
    const QSerialPort::DataBits serialDataBits = QSerialPort::Data8;
    const QSerialPort::Parity serialParity = QSerialPort::NoParity;
    const QSerialPort::StopBits serialStopBits = QSerialPort::OneStop;
    const QSerialPort::FlowControl serialFLowCtrl = QSerialPort::NoFlowControl;

    /* Loop over all serial ports */
    QList<QSerialPortInfo>::iterator i;
    for (i = serialPortNames.begin(); i != serialPortNames.end(); ++i)
    {
        /* Configure the actual serial port */
        serialPort->setPortName(i->portName());
        serialPort->setBaudRate(serialPortBaudRate);
        serialPort->setDataBits(serialDataBits);
        serialPort->setParity(serialParity);
        serialPort->setStopBits(serialStopBits);
        serialPort->setFlowControl(serialFLowCtrl);
        serialPort->blockSignals(true);

        /* If the the selected serial port can be opened*/
        if (serialPort->open(QIODevice::ReadWrite))
        {
            while (serialPort->waitForReadyRead(500)){
                readData = serialPort->readAll();
                readData = readData.left(8);
                standardOutput << QObject::tr("IdacDriverManager::createLibSerialDriver: Data successfully received from port %1")
                                  .arg(i->portName()) << endl;
                /* Validate the return */
                if(hwIdentifier == readData){
                    serialPort->write(writeData);

                    m_driver = serial_driver;
                    serialPort->blockSignals(false);
                    return;
                }
            }
            if (serialPort->error() == QSerialPort::ReadError) {
                standardOutput << QObject::tr("IdacDriverManager::createLibSerialDriver: Failed to read from port %1, error: %2")
                                  .arg(i->portName()).arg(serialPort->errorString()) << endl;
            } else if (serialPort->error() == QSerialPort::TimeoutError && readData.isEmpty()) {
                standardOutput << QObject::tr("IdacDriverManager::createLibSerialDriver: No data was currently available"
                                              " for reading from port %1")
                                  .arg(i->portName()) << endl;
            }
        }
        else {
            standardOutput << QObject::tr("IdacDriverManager::createLibSerialDriver: Failed to open port %1, error: %2")
                              .arg(i->portName()).arg(serialPort->errorString()) << endl;
        }

        /* The unused ports must be closed to prevent an application lock */
        if (NULL == m_driver){
            serialPort->close();
        }
    }

    delete serial_driver;
}
